const faker = require('faker');
const moment = require('moment');
const utils = require('./utils.js');
const deepEqual = require('deep-equal');

//----------------------------------------------------------------------------------------------
// endpoint definitions


function genValue(field) {
    var type = field.type;
    switch (type) {
        case 'pickWeighted':
            var options = field.options;
            return utils.pickWeighted(options);
        case 'pick':
            var options = field.options;
            return utils.pick(options);
        case 'normalInt':
            return utils.genNormalInt(field.mean, field.stdev, field.min, field.max);
        case 'normalFloat':
            var val = utils.genNormal(field.mean, field.stdev, field.min, field.max);
            if (typeof field.toFixed == 'undefined') {
                return val;
            } else {
                return val.toFixed(field.toFixed);
            }
        case 'randomInt':
            return utils.intBetween(field.min, field.max);
        case 'randomFloat':
            var val = utils.floatBetween(field.min, field.max);
            if (typeof field.toFixed == 'undefined') {
                return val;
            } else {
                return val.toFixed(field.toFixed);
            }
        case 'ip':
            return faker.internet.ip();
        case 'email':
            return faker.internet.email();
        case 'userName':
            return faker.internet.userName();
        case 'url':
            return faker.internet.url();
        case 'mac':
            return faker.internet.mac();
        case 'domainName':
            return faker.internet.domainName();
        case 'word':
            return faker.lorem.word();
        case 'sentence':
            return faker.lorem.sentence();
        case 'dateRecent':
            var val = faker.date.recent();
            var mt = new moment(val);
            return mt.format(field.format);
        case 'formattedTimestamp':
            var mt = new moment();
            return mt.format(field.format);
        case 'utcTimestamp':
            var mt = new moment().utc();
            return mt.format();
        case 'fileName':
            return faker.system.fileName();
        case 'fileType':
            return faker.system.fileType();
        default:
        // code block
    }
}

function replaceAll(key, value, string) {
    const field = "{{" + key + "}}";
    const replacer = new RegExp(field, 'g');
    return string.replace(replacer, value);
}

var activeLoggers = {};
var timers = {};

function stopAll() {
    for (const loggerId in activeLoggers) {
        activeLoggers[loggerId].isRunning = false;
        clearTimeout(timers[loggerId]);
    }
    return activeLoggers;
}
exports.stopAll = stopAll;

function getActiveLoggers() {
    return activeLoggers;
}
exports.getActiveLoggers = getActiveLoggers;

function getActiveLoggerIds() {
    var keys = Object.keys(activeLoggers);
    return keys;
}
exports.getActiveLoggerIds = getActiveLoggerIds;

function setLogger(id, logger) {
    var changed = false;
    if( typeof activeLoggers[id] != 'undefined') {
        changed = !deepEqual( activeLoggers[id], logger);
        activeLoggers[id] = logger;
        _serviceConditions.loggers[id] = logger;
    } else {
        changed = true;
        activeLoggers[id] = logger;
        _serviceConditions.loggers[id] = logger;
        runLogger(id);
    }
    return changed;
    
}
exports.setLogger = setLogger;

function runLogger(loggerId) {
    var logger = activeLoggers[loggerId];
    if (logger) {
        var logEntry = genLog(logger);
        utils.log(logEntry);
        var repeat = logger.repeat;
        var time = utils.genNormalInt(repeat.mean, repeat.stdev, repeat.min, repeat.max);
        timers[loggerId] = setTimeout(runLogger, time, loggerId);
    }
}

function removeLogger(loggerId) {
    utils.log('Removing logger', null, "DEBUG");
    clearTimeout(timers[loggerId]);
    delete timers[loggerId];
    delete activeLoggers[loggerId];
    delete _serviceConditions.loggers[loggerId];
}
exports.removeLogger = removeLogger;

function genLog(logger) {
    var fields = logger.fields;
    if( typeof fields == 'undefined' ) return logger.template;

    // otherwise lets make up some stuff
    var fieldNames = Object.keys(fields);
    var template = logger.template;
    for (const fieldName of fieldNames) {
        var field = fields[fieldName];
        var value = genValue(field);
        template = replaceAll(fieldName, value, template);
    }
    return template;
}
exports.genLog = genLog;




