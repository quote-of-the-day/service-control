const urlParser = require('parse-url');
const metrics = require('./metrics.js');
const loggerEngine = require('./logger.js');
const controller = require('./callStatus.js');
const deepEqual = require('deep-equal');
const ip = require('ip');
const utils = require('./utils.js');
const promMid = require('express-prometheus-middleware');

exports.utils = utils;

//==================================================================================================
// Anaomaly Generator config 


var _anomalyGenerator = {};
var _app = null;
var _pollingFrequency = 5000;

exports.config = function(config, app){
    if( typeof _app == 'undefined' ) throw new Error("Configured express app required.");
    _app = app;  // required

    _app.get('/health', function (req, res) {
        setTimeout( () => {
            if( _serviceConditions.healthy ) {
                res.status(200).send("OK");
            } else {
                res.status(500).send("Not feeling that well.");
            }    
        },100); // nominal 100ms delay
    });

    _app.use(promMid());

    _app.get('/serviceConditions', function(req,res){
        res.json( reportCurrentConditions() );
    });

    if( typeof config.logLevel != 'undefined') utils.logLevel(config.logLevel); // optional, defaulted

    // allow service to run even if there is no anomaly generator.  This is of use 
    // when just running the app to generate training data.

    if( typeof config.url != 'undefined' && config.url != "" ) {
        var url = urlParser(config.url);
        if( url.port == null ) url.port = 80;
        _anomalyGenerator = {
            "url": config.url,
            "hostname":  url.resource,
            "port": url.port,
            "path": url.pathname
        }
        if( Number.isInteger(config._pollingFrequency) && config._pollingFrequency>0 && config._pollingFrequency<120000 ){
            _pollingFrequency = config._pollingFrequency;  // optional
        }
        setInterval( getServiceConditions, _pollingFrequency );
    } else {
        utils.log('WARNING: Missing anomaly generator URL. This service will not be able to induce anomalies.',null,'WARN');
    }
}

function reportCurrentConditions(){
    var current = _serviceConditions;
    current.memHogs = metrics.activeMemHogs();
    current.cpuHogs = metrics.activeCpuHogs();
    return current;
}


//==================================================================================================
// Service Conditions 
// This json object represents all the types and ways anomalous behavior can be induced.  At a set
// interval (default every 5 seconds) this application will make a call to the anomaly generator to
// get the latest version of this json object.  If there is an action property on the returned 
// json, then it is acted upon immediately (and not cached as part of the overall service condition).
// 
//

var _default_serviceConditions = {
    "memHogs": 0,
    "cpuHogs": 0,
    "healthy": true,
    "loggers": { },
    "endpoints": { }
};
_serviceConditions = JSON.parse( JSON.stringify(_default_serviceConditions) );


function setDefaultServiceConditions(conditions){
    _default_serviceConditions = conditions;  // TODO: validation
}
exports.setDefaultServiceConditions = setDefaultServiceConditions;


// this function is called every 5 seconds or so, and requests the latest configuration
// from the anomaly generator.

function updateServiceConditions(sc){

    var noChange = deepEqual(sc,_serviceConditions);
    if( noChange ) {
        utils.log("Service conditions unchanged.", null, "DEBUG");
        return;
    }

    // first deal with action
    if( typeof sc.action != 'undefined' && typeof sc.action.type != 'undefined') {
        switch ( sc.action ) {
            case 'crash': return crashService();
            case 'reset': return resetServiceConditions();
        }
    }

    _serviceConditions = sc;

    // now update and check things
    metrics.updateMemoryHogs();
    metrics.updateCpuHogs();

    // sync loggers
    var activeLoggerIds = loggerEngine.getActiveLoggerIds();

    for( var logId in sc.loggers ) {
        // todo: validation, and change
        var logger = sc.loggers[logId];
        if( loggerEngine.setLogger(logId, logger) ) changed = true;
        if( activeLoggerIds.includes(logId) ) {
            activeLoggerIds.splice(logId,1);
        }
    }

    // now remove any loggers left
    if( activeLoggerIds.length > 0 ) changed = true;
    for( var loggerId of activeLoggerIds ) {
        loggerEngine.removeLogger(loggerId);
    }

    // endpoint conditions
    metrics.updateLatencies(sc.endpoints);
    controller.setCallStatusOverrides(sc.endpoints);

    delete _serviceConditions;
    _serviceConditions = sc;
    utils.log("Updated service conditions.", null, "DEBUG");


}

exports.healthy = _serviceConditions.healthy;

function crashService(){
    utils.log("Aaaaah!");
    setTimeout(function () {
        // process.exit(-1);  // produces simpler clear log entries than uncaught exception
        process.nextTick(function () {
            throw new Error;
        });
    }, 3000);
}

var failCount = 0;

function getServiceConditions(){
    const options = {
        "headers": { "Accept": "application/json" },
        "method": 'GET',
        "hostname": _anomalyGenerator.hostname,
        "port": _anomalyGenerator.port,
        "path": _anomalyGenerator.path,
        "timeout": 4000
    }

    if( !_anomalyGenerator.path.includes('?') ) {
        // then lets create an identifier with the local IP address
        var localIp = ip.address();
        options.path += "?instance="+localIp;
    }

    utils.httpRequest(options)
    .then( (sc) => {
        updateServiceConditions(sc);
        failCount = 0;
    })
    .catch( (error) => {
        if( error.status == "404" ){
            setDefaultServiceConditions(_default_serviceConditions);
            utils.log("Problem getting service conditions. Status code 404.  Resetting to local defaults.", null, "DEBUG");
        } else {
            failCount++;    
            if( failCount >= 10 ) {
                utils.log("Problem getting service conditions. " + failCount + " consecutive failed attempts to get service conditions. Resetting to local defaults.", null, "INFO");
                failCount = 0;
                setDefaultServiceConditions(_default_serviceConditions);
            } else {
                utils.log("Problem getting service conditions. " + failCount + " consecutive failed attempts to get serviec conditions.", null,"DEBUG");
            }
        }
    });
}




//==================================================================================================
// Logging 

exports.logLevel = utils.logLevel;

exports.log = utils.log;

//==================================================================================================
// Endpoints





exports.endpointGet = function(path,logMsg,handler){
    var mPath = "GET " + path;
    // _serviceConditions.endpoints[mPath] = {
    //     "latency": {
    //         "mean": 100,
    //         "stdev": 100,
    //         "min": 20,
    //         "max": 200
    //     }
    // }
    _app.get(path, function (req, res) {
        var start = new Date().getTime();
        var delay = metrics.getDelay(mPath);
        var requestToken = req.query.requestToken;

        utils.log(logMsg, requestToken, "INFO");
        utils.log('[' + start + '] Service being delayed: ' + delay + 'ms', requestToken, "DEBUG");

        setTimeout(function () {
            try {

                var override = controller.callStatusOverride(mPath);
                if( override ) {
                    if( typeof override.headers != 'undefined' ) {
                        var keys = Object.keys(override.headers);
                        for( var header of keys ) {
                            res.header(header, override.headers[header] );
                        }
                    }
                    var payload = "";
                    if( typeof override.payload != 'undefined' ) {
                        payload = override.payload;
                    } 
                    res.status(override.code).send(payload);
                    if( typeof override.template != "undefined" ) {
                        var log = loggerEngine.genLog(override);
                        utils.log(log);
                    }
                } else {
                    handler(req,res);
                    runDependentLoggers(mPath); // if there is are dependent loggers then kick them off
                }
            } catch (err) {
                const msg = `Error handling request ${req.path}. Replying with Status: 500. ${err}`;
                utils.log(msg, requestToken, "ERROR");
                res.status(500).send(msg);
            }
        }, delay);
    });
}

exports.endpointPost = function(path,logMsg,handler){
    var mPath = "POST " + path;
    _app.post(path, function (req, res) {
        var start = new Date().getTime();
        var delay = metrics.getDelay(mPath);
        var requestToken = req.query.requestToken;

        utils.log(logMsg, requestToken, "INFO");
        utils.log('[' + start + '] Service being delayed: ' + delay + 'ms', requestToken, "DEBUG");

        setTimeout(function () {
            try {

                var override = controller.callStatusOverride(mPath);
                if( override ) {
                    if( typeof override.headers != 'undefined' ) {
                        var keys = Object.keys(override.headers);
                        for( var header of keys ) {
                            res.header(header, override.headers[header] );
                        }
                    }
                    var payload = "";
                    if( typeof override.payload != 'undefined' ) {
                        payload = override.payload;
                    } 
                    res.status(override.code).send(payload);
                    if( typeof override.log != undefined ) {
                        var log = loggerEngine.genLog(override);
                        utils.log(log);
                    }
                } else {
                    handler(req,res);

                    runDependentLoggers(mPath); // if there is are dependent loggers then kick them off
                }                
            } catch (err) {
                const msg = `Error handling request ${req.path}. Replying with Status: 500. ${err}`;
                utils.log(msg, requestToken, "ERROR");
                res.status(500).send(msg);
            }
        }, delay);
    });
}

function runDependentLoggers(endpoint){
    var endpoints = _serviceConditions.endpoints;
    if( typeof endpoints[endpoint] == 'undefined'
        || typeof endpoints[endpoint].loggers == 'undefined' ) return;

    var loggers = endpoints[endpoint].loggers;

    for( var logId in loggers ) {
        var logger = loggers[logId];
        var log = loggerEngine.genLog(logger);
        if( typeof logger.delay != 'unidentified' ){
            var delay = utils.genNormalInt(logger.delay.mean, logger.delay.stdev, logger.delay.min, logger.delay.max);
            setTimeout( () => { utils.log(log) }, delay);
        } else {
            utils.log(log);
        }
        
    }

}
