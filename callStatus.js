const utils = require('./utils.js');
const loggerEngine = require('./logger.js');
const deepEqual = require('deep-equal');


//-------------------------------------------------------------------------------------------------------------
// Call Distribution Overides

// global set of defined overrides
_callStatusDistribution = {};

exports.setCallStatusOverrides = function(){
    var changed = false;
    var endpointsConfig = _serviceConditions.endpoints;
    var paths = Object.keys(endpointsConfig);
    for(var i=0; i<paths.length; i++){
        var path = paths[i];
        if( typeof _callStatusDistribution[path] != 'undefined' && typeof endpointsConfig[path].responseOverride != 'undefined' ) {
            changed = !deepEqual( endpointsConfig[path].responseOverride, _callStatusDistribution[path]);
            _callStatusDistribution[path] = endpointsConfig[path].responseOverride;
        } else if( typeof _callStatusDistribution[path] !=  typeof endpointsConfig[path].responseOverride) {
            changed = true;
            _callStatusDistribution[path] = endpointsConfig[path].responseOverride;
        }
    }
    return changed;
}

function handleOverride(res, override, requestToken) {
    if( typeof override.headers != 'undefined' ) {
        var keys = Object.keys(override.headers);
        for( var header of keys ) {
            res.header(header, override.headers[header] );
        }
    }
    var payload = "";
    if( typeof override.payload != 'undefined' ) {
        payload = override.payload;
    } 
    res.status(override.code).send(payload);
    if( typeof override.template != 'undefined' ) {
        var log = loggerEngine.genLog(override);
        utils.log(log,requestToken);
    }
}
exports.handleOverride = handleOverride;

function callStatusOverride(path){
    var keys = Object.keys(_callStatusDistribution);
    if( keys.length == 0 || typeof _callStatusDistribution[path] == 'undefined') return null;

    var callStatusDistribution = _callStatusDistribution[path];

    var optionsArray = [];
    for( var option of callStatusDistribution ){

        // do checking here
        var code = option.code;
        var weight = option.weight;
        if( code <= 563 && option.weight >= 0 ) {
            for(var j=0;j<weight;j++){
                optionsArray.push(option);
            }
        }
    }

    if( optionsArray.length > 0 ) {
        var len = optionsArray.length;
        var i = utils.intBetween(0,len);
        var override = optionsArray[i]; 
        if( override.code <= 0 ) override = null;
        return override;
    } else {
        return null;
    }

}

exports.callStatusOverride = callStatusOverride;

function validationErrors(override){

    // todo

    return null;
}


