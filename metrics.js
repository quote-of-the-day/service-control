// const e = require('express');
const utils = require('./utils.js');

var endpointLatency = { };


// this function computes the delay used to induce latency in all the 
// application services (not /metrics, liveness probe or any of the 
// serviceController services)
function getDelay(path) {
    if( typeof endpointLatency[path] == 'undefined' ) {
        return utils.genNormalInt(100);
    } else {
        var mean = endpointLatency[path].mean;
        var stdev = endpointLatency[path].stdev;
        var min = endpointLatency[path].min;
        var max = endpointLatency[path].max;
        return utils.genNormalInt(mean, stdev, min, max);
    }

}
exports.getDelay = getDelay;

exports.updateLatencies = function updateLatencies(endpoints){

    var paths = Object.keys(endpoints);
    for(var i=0; i<paths.length; i++){
        var path = paths[i];
        endpointLatency[path] = endpoints[path].latency;
    }

}


// --------------------------------------------------
// Memory

var memHogs = [];
var updatingMemoryHogs = false;

exports.activeMemHogs = function(){
    return memHogs.length;
}

// TODO : this is working for now, but it is a little messed up, and
// if there is time, should be re-worked, such that it is resilient
// to changes in target before the first request to change is finished.
exports.updateMemoryHogs = function(){
    if( updatingMemoryHogs ) return; // 

    var currentHogs = memHogs.length;
    var diff = _serviceConditions.memHogs - currentHogs;
    if( diff > 0  ) {
        // then add more
        utils.log( "Adding mem hogs: "+ diff,null,"DEBUG");
        updatingMemoryHogs = true;
        for(var i=0; i<diff; i++ ){
            setTimeout( () => {
                allocMemoryBlock();
            }, i*1000);
        }
        setTimeout( () => {
            updatingMemoryHogs = false;
        }, diff*1000);
    } else if( diff < 0 ) {
        diff = -diff;
        utils.log( "Removing mem hogs: "+ diff,null,"DEBUG");
        if( diff >= memHogs.length ) diff = memHogs.length;
        updatingMemoryHogs = true;
        for(var i=0; i<diff; i++ ){
            setTimeout( () => {
                freeMemoryBlock();
            }, i*1000);
        }
        setTimeout( () => {
            updatingMemoryHogs = false;
        }, diff*1000);
    }
}

function freeMemoryBlock(){
    var currentHogs = memHogs.length;
    if( _serviceConditions.memHogs < currentHogs) {
        memHogs.pop();
        utils.log("hot dog mem hog ["+ memHogs.length + "], taking out garbage",null,"DEBUG");
        global.gc();
    }
}

function allocMemoryBlock(){
    if( _serviceConditions.memHogs > memHogs.length) {
        const arr = []
        var blockSize = 15000 * 1024 / 8
        arr.length = blockSize;
        for (let i = 0; i < blockSize; i++) {
            arr[i] = i;
        }
        memHogs.push(arr);
        var len = memHogs.length-1;
        utils.log("new mem hog ["+ len + "]",null,"DEBUG");
    }
}



// --------------------------------------------------
//  CPU

var CPU_HOG_SIZE = 100000;
var cpuHogs = [];

exports.activeCpuHogs = function(){
    return cpuHogs.length;
}

function cpuHogBurst(){
    var countTo = CPU_HOG_SIZE;
    var result;
    for( var i=0; i<countTo; i++){
        result = Math.random() * Math.random();
    }
    utils.log("CPU bursted",null,"DEBUG");
}

function addCpuHog() {
    var cpuInterval = setInterval(cpuHogBurst,500);
    cpuHogs.push(cpuInterval);    
    utils.log('Adding cpu hog [' + cpuHogs.length + ']',null,"DEBUG");
};

function removeCpuHog() {
    if( cpuHogs.length > 0 ) {
        var hog = cpuHogs.pop();
        clearInterval(hog);
        delete hog;
        utils.log('Removed cpu hog [' + cpuHogs.length + ']',null,"DEBUG");
    }
};

exports.updateCpuHogs = function updateCpuHogs(){
    var hogs = _serviceConditions.cpuHogs;
    var currentCpuHogCount = cpuHogs.length;
    var diff = hogs - currentCpuHogCount;
    if( diff > 0 ) {
        utils.log("Adding cpu hogs: " + diff ,null,"DEBUG");
        for( i=0; i<diff; i++){
            setTimeout( ()=> {
                addCpuHog();
            }, i*500);
        }
    } else if( diff < 0 ) {
        diff = -diff;
        utils.log("Removing cpu hogs: " + diff ,null,"DEBUG");
        for( var i=0; i<diff; i++){
            setTimeout( ()=> {
                removeCpuHog();
            }, i*500);
        }
    }
}






